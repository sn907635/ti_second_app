/**
 * @author Nick.H
 * 包裏視圖內容，要呈現在視圖容器內容
 */
function wrapContent(views)
{
    var wrappedContent = Ti.UI.createView(
    {
        layout:"vertical",
        width:Ti.UI.FILL,
        height:Ti.UI.FILL        
    }
    );
    for(idx in views)
    {
        wrappedContent.add(views[idx]);   
    }    
    return wrappedContent;
}
exports.wrapView = wrapContent;
