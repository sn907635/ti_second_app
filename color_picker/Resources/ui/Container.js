/**
 * @author Nick.H
 * 容器視圖，顯示其它UI元件
 */
var win;
/**
 * 取得容器視圖，內含要被顯示的UI元件
 * @param {View} wrapView
 */
function getContainerView(wrapView)
{
    win = Ti.UI.createWindow(
    {
        navBarHidden:true,
        modal:true,
        backgroundColor:"#fff"
    }    
    );
    win.add(wrapView);
    return win;
}
exports.getContainerView = getContainerView;
