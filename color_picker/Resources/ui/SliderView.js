/**
 * @author Nick.H
 * UI元件(Slider)，控制R G B三色
 */
var sliderR;
var sliderG;
var sliderB;
var sliders = [];
var parentWin;
var defaultValue = {
    max:255,
    value:255,
    width:Ti.UI.FILL,
    height:Ti.UI.SIZE 
};
/**
 * 初始值(白)
 * RGB = 255
 */
var red = 255;
var blue = 255;
var green = 255;
function getSliderView()
{
    /**
     * Slider元件初始化
     */
    sliderR = Ti.UI.createSlider(defaultValue);
    sliderG = Ti.UI.createSlider(defaultValue);
    sliderB = Ti.UI.createSlider(defaultValue);
    sliderR.setBackgroundColor("red");
    sliderG.setBackgroundColor("green");
    sliderB.setBackgroundColor("blue");
    sliderR.color = "red";
    sliderB.color = "blue";
    sliderG.color = "green";
    sliderR.addEventListener("change",changeHandler);
    sliderG.addEventListener("change",changeHandler);
    sliderB.addEventListener("change",changeHandler);
    sliders.push(sliderR);
    sliders.push(sliderG);
    sliders.push(sliderB);
    return sliders;
}
/**
 * Slider事件處理
 * @param {Object} e
 */
function changeHandler(e)
{    
    switch(e.source.color)
    {
        case "red":
            red = e.value;    
        break;
        case "blue":
            blue = e.value;
        break;
        case "green":
            green = e.value;
        break;
    }    
    changeBkgColor();
}
/**
 * 改變背景顏色
 */
function changeBkgColor()
{            
    bkgColor = "#"+decimalToHexString(Math.round(red))
                  +decimalToHexString(Math.round(green))
                  +decimalToHexString(Math.round(blue));                             
    parentWin.setBackgroundColor(bkgColor);   
}
/**
 * 
 * @param {Object} parent
 */
function setParentView(parent)
{
    parentWin = parent;
}
/**
 * 進制轉換
 * @param {Object} number
 */
function decimalToHexString(number)
{
    if (number < 0)
    {
        number = 0xFFFFFFFF + number + 1;
    }
    if(number == 0)
    {
        return "00";
    }
    return number.toString(16).toUpperCase();
}
exports.getSliderView = getSliderView;
exports.setParentView = setParentView;
